import {Option} from './option';
export class Questions {
    id: number;
    question: string;
    options: Option[];

    constructor(data: any) {
        this.id = data.id;
        this.question = data.questions;
        this.options.forEach(optionResponse => {
            var newOption = new Option(optionResponse);
            this.options.push(newOption);
        })
    }
}

