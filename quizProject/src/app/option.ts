export class Option {
    id: number;
    answer: string;
    isCorrect: boolean;
    selected: boolean; //to know selcted or not 

    constructor(data: any){
        this.id = data.id; 
        this.answer = data.answer;
        this.isCorrect = data.isCorrect;
    }
}
