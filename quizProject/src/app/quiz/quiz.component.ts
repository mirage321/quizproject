import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Questions } from '../questions';
import { Option } from '../option';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css'],
  providers:[QuizService]
})
export class QuizComponent implements OnInit {

  mode = 'quiz';
  questions: [Questions];


  constructor(private quizService: QuizService) { }

  ngOnInit() {
    this.quizService.getAllQuestions().subscribe(response => this.questions = response);
    this.mode = 'quiz';
  }
  onSelect(questions: Questions, option: Option){
    questions.options.forEach(eachOption => {
      if (eachOption.id != option.id) {
        eachOption.selected = false;
      }else{
        eachOption.selected = true
      }
    })
  }

  isCorrectAnswer(questions: Questions){
    if (questions.options.every(eachOption => (eachOption.selected === eachOption.isCorrect))){
      return 'correct';

    } else {
      return 'wrong';
    }
  }
    
  onSubmit() {
    this.mode = 'result';
  }
}
