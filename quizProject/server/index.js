var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoClient = require('mongodb').MongoClient;;

var app = express();
app.use(cors());
app.use(bodyParser());

app.get('/questions/', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/quizDB', function(err, db){
        if (!err){
            db.collection('Questions').find().toArray(function(err,questions){
                resp.send(JSON.stringify(questions));
            });
        }
    });

});

app.listen(9000, () => console.log('@Mirage Nepal Quiz ApI started listening...'));